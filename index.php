<?php

require __DIR__ . '/vendor/autoload.php';

use Sdk\PayRetailers\Models\PayWallAuthentication;
use Sdk\PayRetailers\PayRetailersSdk;
use Sdk\PayRetailers\Security\AESProvider;

$encriptProvider = new AESProvider();
$sdk = new PayRetailersSdk($encriptProvider);

// Create Paywall auth.
$payWallAuth = new PayWallAuthentication(
    $_ENV['USERNAME'],
    $_ENV['PASSWORD'],
    $_ENV['KEY']
);

// Test CreatePayWall.
// $encriptedData = "PCwucs2pnmaDB09ue7a9lBWOh1AIiuuc4Rf2BsZv6W+3fnDsrkKIyK0FWMlXoUEr06oISdh9cv/6u0Mav9EM0UFrYz5Ewt2wUTaoibszu4ko5JSIRxIApyctXy3VDFQh+s3PfR/zDRbyUReJPnsYbWP5rk0oDsfHgeknLgSWoqULmsP47a2xWgtqq93IPLuaiTrt6M6Z5DY/4vr17WNYy364RSx45Cgmz2BrNoZiGvKZYt91yVt6DiHq6aOF8BhTWAePVBy0OGksYBZ43Wo/1PW7sKuBbBG9/hKVZP2JsxairYAcnl6GUNxB3XEA4r0g74ZyFLxn1RQY3L1pAFeqNnzSUfoLhoHa24FWf1YU7VqpCt9F60wkRogJwuZ9ojkbsMqQuQAmganiDx9oE7kro5aEYymnPITRzJQXAD+WlXBs00YHmgXRcF2b83Q36JdoarOofkvLggdCH0WOnrL9LD9uaI0MECSWDFGgxX+BGZQiNyZbf4DUcjz5Hz2+qLZhRIlx78RzVm5057ixorS+TUMw9yEREgCHvlU5pR92S+A2UACcMeEX3Z46TmXnNzNKfCZzemiTjTbF5hyrG5a9DZ//OlAOQnT/rAU+Lg07SwpPQbK5ovQZktJVGieMheukd8qNJqrIy02jDtGBmLyoMtzjk+hekgGBhYiYcmeLwhYmJajRiXzYhCE0zj8OV9ru5dJRb4pBpWkCDfTXOEg/zRqbWd+focM8sE2SLKxwGg3JxgBaVV8v78Xkt719wiHpp4HTQ4iTi66o5gVkXzLY+Hb64e3ROJ+nQfj/bZO2/BNKJEzlbsADALHKhTQ=586";
// $sdk->CreatePayWall($payWallAuth, $encriptedData);

// Test GetPaymentMethods.
$encriptedData = "TmzT2llnlrNcRpMF65qFYdYpWmgyoY8o3jzdctKRTNcJ+zm2DgTOxDgrVX5DY6MjS8Al5VVf78EunR2uhYNuuw==jiwrF6xH6JRjTXkR975";
$sdk->GetPaymentMethods($payWallAuth, $encriptedData);



