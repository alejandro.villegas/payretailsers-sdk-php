<?php

namespace Sdk\PayRetailers\Security;

class EncryptBehaviorAdapter
{
    protected EncryptProviderInterface $provider;
    public function __construct(EncryptProviderInterface $provider)
    {
        $this->provider = $provider;
    }
    /**
     * Generate newrandom String for number and specific characters.
     *
     * @param [type] $num
     * @param [type] $characters
     * @return void
     */
    public function GenerateRandomString($num, $characters)
    {
        return substr(str_shuffle($characters), 0, $num);
    }

    /**
     * Decrypt data by key.
     *
     * @param string $encryptedData
     * @param string $key
     * @return array
     */
    public function requestDecrypt(string $encryptedData, string $key)
    {
        $number = substr($encryptedData, strlen($encryptedData) - $_ENV['RANDON_NUMBER_LENGTH'], $_ENV['RANDON_NUMBER_LENGTH']);

        // Generate ramdom iv.
        $iv = null;

        if ((intval($number) % 2) == 0) {
            $iv = substr($encryptedData, 0, $_ENV['RANDOM_CHARACTER_LENGTH']);
        } else {
            $iv = substr($encryptedData, strlen($encryptedData) - ($_ENV['RANDON_NUMBER_LENGTH'] + $_ENV['RANDOM_CHARACTER_LENGTH']), $_ENV['RANDOM_CHARACTER_LENGTH']);
        }

        $ivEncrypted = $iv;

        // Replace iv and number with empty.
        $encryptedData = str_replace($iv, "", $encryptedData);
        $encryptedData = str_replace($number, "", $encryptedData);

        $JSON_Decrypted = $this->provider->decrypt($encryptedData, $key, $ivEncrypted);

        return $JSON_Decrypted;
    }


    /**
     * Encrypt data by key.
     *
     * @param string $data
     * @param string $key
     * @return string
     */
    public function requestEncrypt(string $plainText, string $key)
    {

        $ivEncrypted = self::GenerateRandomString($_ENV['RANDOM_CHARACTER_LENGTH'], $_ENV['CHARACTER_STRING_RANDOM']);
        $number = self::GenerateRandomString($_ENV['RANDON_NUMBER_LENGTH'], $_ENV['CHARACTER_NUMBER_RANDOM']);
        $JSON_Encrypted = null;

        // Make a encryptation.
        $result = $this->provider->encrypt($plainText, $key, $ivEncrypted);

        // Change position if is odd.
        if (intval($number) % 2 == 0) {
            $JSON_Encrypted = $ivEncrypted . $result . $number;
        } else {
            $JSON_Encrypted = $result . $ivEncrypted . $number;
        }

        return $JSON_Encrypted;
    }
}
