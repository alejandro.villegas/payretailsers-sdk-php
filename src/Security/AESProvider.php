<?php

namespace Sdk\PayRetailers\Security;

class AESProvider implements EncryptProviderInterface
{
    /**
     * Decript cipther text by key and iv.
     *
     * @param [type] $cipherText
     * @param [type] $key
     * @param [type] $iv
     * @return mixed
     */
    public function decrypt($cipherText, $key, $iv)
    {
        $base64Text = base64_decode($cipherText);
        $data = openssl_decrypt($base64Text, 'aes-256-cbc', $key, true, $iv);

        return json_decode($data, true);
    }

    /**
     * Encrypt plain text by key and iv.
     *
     * @param [type] $plainText
     * @param [type] $key
     * @param [type] $iv
     * @return mixed
     */
    public function encrypt($plainText, $key, $iv)
    {
        $encrypted_data = openssl_encrypt($plainText, 'aes-256-cbc', $key, true, $iv);
        return base64_encode($encrypted_data);
    }
}
