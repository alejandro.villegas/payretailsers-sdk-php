<?php

namespace Sdk\PayRetailers\Security;

interface EncryptProviderInterface
{
    public function encrypt($plainText, $key, $iv);
    public function decrypt($cipherText, $key, $iv);
}
