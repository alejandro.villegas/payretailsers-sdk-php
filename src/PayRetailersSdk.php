<?php

declare(strict_types=1);

namespace Sdk\PayRetailers;

use Sdk\PayRetailers\Models\PayWallAuthentication;
use Sdk\PayRetailers\Security\RequestResponseBehaviorAdapter;
use Sdk\PayRetailers\Utils\Paths;
use Dotenv\Dotenv;
use JsonMapper;
use LDAP\Result;
use Sdk\PayRetailers\Models\MapperAdapter;
use Sdk\PayRetailers\Models\MapperUtil;
use Sdk\PayRetailers\Models\PaymentMethods;
use Sdk\PayRetailers\Models\PayRetailersAuthInterface;
use Sdk\PayRetailers\Security\EncryptBehaviorAdapter;
use Sdk\PayRetailers\Security\EncryptProviderInterface;
use Sdk\PayRetailers\Utils\DevUtiles;

final class PayRetailersSdk
{
    private PayRetailersClientBuilder $clientBuilder;
    private EncryptBehaviorAdapter $encryptAdapter;

    public function __construct(EncryptProviderInterface $encryptProvider)
    {
        // Init all env variables.
        $this->initEnv();

        // Init client builder.
        $basePath = Paths::GetBaseEndpointPath();
        $version = $_ENV['VERSION'];

        $this->clientBuilder = new PayRetailersClientBuilder($basePath, $version);

        // Init Encrypt and Decrypt Provider.
        $this->encryptAdapter =  new EncryptBehaviorAdapter($encryptProvider);
    }

    /**
     * Init all env variables.
     *
     * @return void
     */
    private function initEnv()
    {
        $baseDir = dirname(__DIR__);
        $dotenv = Dotenv::createImmutable($baseDir);
        $dotenv->safeLoad();
    }

    /**
     * Set basic auth to SDK.
     *
     * @param PayRetailersAuthInterface $auth
     * @return void
     */
    public function setBasicAuth(PayRetailersAuthInterface $auth)
    {
        $this->clientBuilder->setBasicAuth($auth->getUserName(), $auth->getPassword());
    }

    /**
     * Create PayWall by PayWallAuthentication and data.
     *
     * @param PayWallAuthentication $payWallAuth
     * @param string $encryptedData
     * @return void
     */
    public function CreatePayWall(PayWallAuthentication $payWallAuth, string $encryptedData)
    {
        try {
            // Get payWall info.
            $payWallInfo = $this->encryptAdapter->requestDecrypt($encryptedData, $payWallAuth->key);

            // Make basic auth.
            $this->setBasicAuth($payWallAuth);

            // Call a create paywall post.
            $result = $this->clientBuilder->sendPost(Paths::CreatePayWallPath(), $payWallInfo);
        } catch (\Exception $e) {
            throw $e;
        }

        // Return encrypted data.
        return $this->encryptAdapter->requestEncrypt($result, $payWallAuth->key);
    }

    /**
     * Get all Pay Retailers Payment Methods.
     *
     * @param PayWallAuthentication $payWallAuth
     * @param string $encryptedData
     * @return void
     */
    public function GetPaymentMethods(PayWallAuthentication $payWallAuth, string $encryptedData)
    {
        try {
            // Obtener paywall id.
            $payWallId = $this->encryptAdapter->requestDecrypt($encryptedData, $payWallAuth->key);

            // Make basic auth.
            $this->setBasicAuth($payWallAuth);

            // Call a create paywall get.
            $result = $this->clientBuilder->sendGet(Paths::PayWallPaymentMethodsPath($payWallId['payWallId']));

            $mapper = new MapperAdapter(new PaymentMethods());
            $mappedData = json_encode($mapper->getMappedData(json_decode($result)));
        } catch (\Exception $e) {
            throw $e;
        }

        DevUtiles::dump($this->encryptAdapter->requestEncrypt($mappedData, $payWallAuth->key));

        // Return encrypted data.
        return $this->encryptAdapter->requestEncrypt($mappedData, $payWallAuth->key);
    }
}
