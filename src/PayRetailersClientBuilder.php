<?php

declare(strict_types=1);

namespace Sdk\PayRetailers;

use GuzzleHttp\Client as GuzzleHttpClient;

final class PayRetailersClientBuilder
{
    private GuzzleHttpClient $httpClient;

    private array $headers;

    private array $auth;

    private array $baseUrl;

    private string $version;

    private array $options;

    /**
     * Contructor class
     *
     * @param string $basePath
     */
    public function __construct(string $baseUrl = null, string $version = 'v1')
    {
        $this->headers = [];
        $this->baseUrl = [];
        $this->auth = [];
        $this->options = [];

        // Set Common Header.
        $this->setCommonHeaders();

        // Set base path if exist.
        if (!empty($baseUrl)) {
            $this->version = $version;
            $this->setBaseUrl($baseUrl, $version);
        } else {
            $this->version = $_ENV['VERSION'];
            $this->setBaseUrl($_ENV['PATH_API_PAYRETAILERS'], $_ENV['VERSION']);
        }

        // DevUtiles::dump($this->baseUrl);
        $this->httpClient = new GuzzleHttpClient($this->baseUrl);
    }

    /**
     * Get the value of httpClient
     *
     * @return  GuzzleHttpClient
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * Set the value of httpClient
     *
     * @param  GuzzleHttpClient  $httpClient
     *
     * @return  self
     */
    public function setHttpClient(GuzzleHttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Get the value of headers
     *
     * @return  array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Set the value of headers
     *
     * @param  array  $headers
     *
     * @return  self
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    /**
     * Set common headers.
     *
     * @return void
     */
    public function setCommonHeaders()
    {
        $this->headers += [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'charset' => 'utf-8'
        ];
    }

    /**
     *  Add new element to header list.
     *
     * @param string $header
     * @return void
     */
    public function addHeader(string $header)
    {
        $this->headers[] = $header;
    }

    /**
     * Set Basic Auth
     *
     * @param string $user
     * @param string $password
     * @return void
     */
    public function setBasicAuth(string $user, string $password)
    {
        $this->auth['auth'] = [$user, $password];
    }

    /**
     * Set Base Path Url.
     *
     * @param string $basePath
     * @param string $version
     * @return void
     */
    public function setBaseUrl(string $basePath, string $version)
    {
        $this->baseUrl = ['base_uri' => $basePath . "/$version/"];
    }

    /**
     * Send a Post Request.
     *
     * @param string $url
     * @param array $body
     * @return mixed
     */
    public function sendPost(string $url, array $body)
    {
        try {
            $options = $this->generateOptions();
            $options += ['json' => $body];

            $response = $this->httpClient->request('POST', $url, $options);
        } catch (\Exception $e) {
            return [
                'error' => TRUE,
                'code' => $response->getStatusCode(),
                'response' => $response->getBody()->getContents()
            ];
        }

        return $response->getBody()->getContents();
    }

    /**
     * Send a Get Request.
     *
     * @param string $url
     * @param array $query
     * @return mixed
     */
    public function sendGet(string $url, array $query = [])
    {
        try {
            $options = $this->generateOptions();
            $options += ['query' => $query];
            $response = $this->httpClient->request('GET', $url, $options);
        } catch (\Exception $e) {
            throw $e;
        }

        return $response->getBody()->getContents();
    }

    public function generateOptions()
    {
        $options = [];

        // Authorization.
        if (count($this->auth) > 0) {
            $options += $this->auth;
        }
        // Set Headeres.
        if (count($this->headers) > 0) {
            $options += $this->headers;
        }

        return $options;
    }
}
