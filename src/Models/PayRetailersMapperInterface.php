<?php

namespace Sdk\PayRetailers\Models;

interface PayRetailersMapperInterface
{
    public function map(array $list);
}
