<?php

namespace Sdk\PayRetailers\Models;

interface PayRetailersAuthInterface
{
    public function getUserName();
    public function setUserName(string $userName);
    public function getPassword();
    public function setPassword(string $password);
    public function getKey();
    public function setKey(string $key);
}
