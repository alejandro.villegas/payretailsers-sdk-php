<?php

namespace Sdk\PayRetailers\Models;

class MapperAdapter
{
    protected PayRetailersMapperInterface $mapped;

    public function __construct(PayRetailersMapperInterface $mapped)
    {
        $this->mapped = $mapped;
    }

    public function getMappedData($json)
    {
        return $this->mapped->map($json);
    }
}
