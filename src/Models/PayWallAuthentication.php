<?php

namespace Sdk\PayRetailers\Models;

class PayWallAuthentication implements PayRetailersAuthInterface
{
    public string $userName;
    public string $password;
    public string $key;

    /**
     * Constructor class.
     */
    public function __construct($userName = "", $password = "", $key = "")
    {
        $this->userName = $userName;
        $this->password = $password;
        $this->key = $key;
    }

    /**
     * Get the value of userName
     *
     * @return  string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set the value of userName
     *
     * @param  string  $userName
     *
     * @return  self
     */
    public function setUserName(string $userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get the value of password
     *
     * @return  string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @param  string  $password
     *
     * @return  self
     */
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of key
     *
     * @return  string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set the value of key
     *
     * @param  string  $key
     *
     * @return  self
     */
    public function setKey(string $key)
    {
        $this->key = $key;

        return $this;
    }
}
