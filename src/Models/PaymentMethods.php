<?php

namespace Sdk\PayRetailers\Models;

use Sdk\PayRetailers\Utils\DevUtiles;

class PaymentMethods implements PayRetailersMapperInterface
{
    public function map(array $json)
    {
        return [
            "paymentMethodInfo" => $json
        ];
    }
}
