<?php

namespace Sdk\PayRetailers\Utils;

class Paths
{
    public static function GetBaseEndpointPath()
    {
        return $_ENV['PATH_API_PAYRETAILERS'];
    }

    public static function CreatePayWallPath()
    {
        return $_ENV['PATH_API_PAYWALL'];
    }

    public static function PayWallPaymentMethodsPath(string $payWallId)
    {
        return self::CreatePayWallPath() . "/$payWallId/" . $_ENV['PATH_API_PAYMENT_METHODS'];
    }

    public static function TransactionFieldValidationPath(string $payWallId, string $paymentMethodId)
    {
        return self::PayWallPaymentMethodsPath($payWallId) . "/$paymentMethodId/" . $_ENV['PATH_API_FIELDS'];
    }

    public static function CreateTransaction()
    {
        return self::CreatePayWallPath() . $_ENV['PATH_API_FIELDS'];
    }
}
