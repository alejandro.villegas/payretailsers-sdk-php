<?php

namespace Sdk\PayRetailers\Utils;

class DevUtiles
{
    /**
     * Dumps data.
     *
     * @param [type] ...$args
     * @return void
     */
    public static function dump(...$args)
    {
        echo '<pre>';
        var_dump($args);
        die();
    }
}
